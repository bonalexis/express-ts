FROM node:20.10.0-bookworm-slim AS build

WORKDIR /build

COPY package*.json .
RUN npm install && npm install typescript -g
COPY . .
RUN npm run build



FROM node:20.10.0-bookworm-slim AS app

WORKDIR /app

ENV NODE_ENV production

COPY package*.json .
RUN npm install --only=production
COPY --from=build /build/dist build

USER node
CMD ["node", "build/server.js"]
