# Express TS

API Express utilisant TypeScript.

## Premiers pas

### Configuration des variables d'environnement

Dans un premier temps il faut copier le fichier des variables.

```bash
cp .env.example .env
```

## Commandes

Lancement du serveur de développement.

```bash
npm run dev
```

Vérification de la bonne structure du code.

```bash
npm run lint
```

Correction des avertissements dans la structure du code.

```bash
npm run format
```

Compilation du projet vers le répertoire `dist`.

```bash
npm run build
```

Déploiement du projet compilé.

```bash
npm start
```

## Architecture

```txt
├─ src
│   ├─ server.ts
│   └─ app
└─ dist
```

```mermaid
flowchart BT
    subgraph Web layer
    routes
    controllers
    middlewares
    end

    subgraph Service layer
    services
    end

    subgraph Data access layer
    models
    end

    server[[server.ts]] --> app
    app --> routes
    routes --> middlewares
    routes --> controllers
    controllers --> middlewares
    controllers --> services
    services --> models
    models --> bdd[(BDD)]
```

## CommitLint

- feat: (nouvelle fonctionnalité pour l'utilisateur, pas une nouvelle fonctionnalité pour le script de build)
- fix: (correction d'un bug pour l'utilisateur, pas une correction d'un script de build)
- docs: (modifications de la documentation)
- style: (formatage, points-virgules manquants, etc ; aucun changement de code de production)
- refactor: (refactoriser le code de production, par exemple renommer une variable)
- test: (ajout de tests manquants, refactorisation des tests ; aucun changement de code de production)
- chore: (mise à jour des tâches grunt, etc. ; aucun changement de code de production)
