import { Handler } from "express";
import sayHello from "../services/sayHello";

const HelloTo: Handler = (req, res) => {
  const name = req.params.name;
  const message = sayHello(name);

  res.json({ message });
};

export { HelloTo };
