import { Handler } from "express";
import sayHello from "../services/sayHello";

const HelloWorld: Handler = (req, res) => {
  const message = sayHello();
  res.json({ message });
};

export { HelloWorld };
