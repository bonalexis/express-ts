import express, { Router } from "express";
import * as controller from "../controllers/hello.controller";

const helloRouter: Router = express.Router();

helloRouter.get("/:name", controller.HelloTo);

export default helloRouter;
