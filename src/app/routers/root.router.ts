import express, { Router } from "express";
import * as controller from "../controllers/root.controller";

const rootRouter: Router = express.Router();

rootRouter.get("/", controller.HelloWorld);

export default rootRouter;
