import express, { Application } from "express";
import helloRouter from "./routers/hello.router";
import logMiddleware from "./middlewares/log.middleware";
import rootRouter from "./routers/root.router";

const app: Application = express();

// Middlewares
app.use(logMiddleware);

// Routes
app.use("/", rootRouter);
app.use("/hello", helloRouter);

export default app;
