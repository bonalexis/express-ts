import { Handler } from "express";

const logMiddleware: Handler = (req, res, next) => {
  console.log(req.path);
  next();
};

export default logMiddleware;
