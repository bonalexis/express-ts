const sayHello = (name: string = "World") => {
  return `Hello ${name}!`;
};

export default sayHello;
